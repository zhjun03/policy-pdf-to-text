<?php

namespace PolicyPdf;

/**
 * Author:Robert
 *
 */
class Analysis
{
    public $brandMap = [
        "PICC",
        "PingAn",
        "JinTai",
        "CPIC",
        "ALL_TRUST",
        "CHINA_COAL",
        "YAIC",
        "EDHIC",
        "CHINATP",
        "CHAMPION",
        "CPCI",
        "ACIC",
        "DAJIA",
        "LIBERTY",
        "APPC",
        "TAIC",//天安
        "HAIC",//华安
        "CICP",//中华联合
        "ZhuFeng",//珠峰
    ];

    public $error;

    public $data;

    public $brand;

    public function analysis($filePath, $brand)
    {
        if (!in_array($brand, $this->brandMap)) {
            $this->error = '品牌不在规定范围';
            return false;
        }
        //检查PDF文件地址
        if (!file_exists($filePath)) {
            $this->error = '相关文档不存在';
            return false;
        }
        $data = shell_exec('pdftotext -layout '.$filePath.' - ');
        if (!$data) {
            $this->error = '解析数据为空,请检查是否是PDF文档';
            return false;
        }
        $this->brand = $brand;
        $this->data = $data;
        $class = 'PolicyPdf\\'.$this->brand;
        $brandAnalysisModel = new $class();
        $brandAnalysisData = $brandAnalysisModel->analysis($this->data);
        if ($brandAnalysisData == false) {
            $this->error = $brandAnalysisModel->error;
            return false;
        }
        return $brandAnalysisModel->getInfoArr();
    }

}