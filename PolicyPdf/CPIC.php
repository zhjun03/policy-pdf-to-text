<?php
/**
 * Author:Robert
 *
 */

namespace PolicyPdf;

class CPIC extends BaseAnalysisInfo
{
    public function analysis($data = '')
    {
        if (!$data) {
            $this->error = '数据不能为空';
            return false;
        }
        if (!preg_match('/中国太平洋财产保险/', $data)) {
            $this->error = '不是太平洋保单';
            return false;
        }
        //确定是商业险还是交强险
        $type = '';
        if (preg_match("/交通事故责任/", $data)) {
            $type = "TCI";
        } else {
            $type = "VCI";
        }
        $this->type = $type;
        //保单号、保险单号： PDAA202151010001095915
        if (preg_match("/保险单号[：]*[\s]*(\w*)/i", $data, $matches) && !empty($matches[1])) {
            $this->policyNo = $matches[1];
        }
        //支付日期 收费确认时间：2021-11-11 10:20
        if (preg_match("/收费确认时间[：|:]*(\d+[\-|\/]\d+[\-|\/]\d+[\s]*\d+\:\d+\:\d+)/", $data, $matches) && !empty($matches[1])) {
            $this->paidAt = $matches[1];
        }
        //被 保 险 人 钟世奎
        if (preg_match("/被[\s]*保[\s]*险[\s]*人[\s]*[：|:]*[\s]*([\x7f-\xff]*\.*[\x7f-\xff]*)[\s]*/i", $data, $matches) && !empty($matches[1])) {
            $this->recognizee = $matches[1];
        }
        //   地 址     柏合1组
        if (preg_match("/地[\s]*址[\s]*[：]*(\S*)[\s]*?/i", $data, $matches) && !empty($matches[1])) {
            $this->recognizeeAddress = $matches[1];
        }
        //车牌号、号 牌 号 码     川A2A210                          厂 牌 型 号
        if (preg_match("/号[\s]*牌[\s]*号[\s]*码[：]*[\s]*([\x7f-\xff]*[\*]*\w*[-]*[\*]*\w*)/i", $data, $matches) && !empty($matches[1])) {
            $this->licensePlateNo = $matches[1];
        }
        //机动车种类、机动车种类           客车
        if (preg_match("/机动车种类[：]*[\s]*([\x7f-\xff]*\w*[\x7f-\xff]*)/i", $data, $matches) && !empty($matches[1])) {
            $this->vehicleOwnerType = $matches[1];
        }
        //使用性质、使用性质       家庭自用汽车
        if (preg_match("/使[\s]*用[\s]*性[\s]*质[：]*[\s]*([\x7f-\xff]*)[\s]*?/i", $data, $matches) && !empty($matches[1])) {
            $this->vehicleUsedNature = $matches[1];
        }
        //发动机号、 发 动 机 号      2035030
        if (preg_match("/发[\s]*动[\s]*机[\s]*号[\s]*[码]*[：]*[\s]*(\w*)[\s]*/i", $data, $matches) && !empty($matches[1])) {
            $this->engineNo = $matches[1];
        }
        //车架号、 VIN码/车架号    LVHRM4857D5023506/LVHRM4857D5023506
        if (preg_match("/车架号[：]*[\）|\)]*[\s]*(\w*)[\/]*?/i", $data, $matches) && !empty($matches[1])) {
            $this->vin = $matches[1];
        }
        //厂牌型号、厂 牌 型 号       思威DHW6453R4ASD多用途乘用车 奥迪AUDI A7 3.0TFSI QUATTRO轿
        if (preg_match("/厂[\s]*牌[\s]*型[\s]*号[\s]*[：]*([\x7f-\xff]*[\s*\w*\.*\-*]*[\x7f-\xff]*)\s?/i", $data, $matches) && !empty($matches[1])) {
            $this->modelCode = $matches[1];
            //名称太长,占据两行的
            if (preg_match("/绝对免赔额[：]*[\/]*[\s]*([\w*\.*]*[\x7f-\xff]*)[\s]*?/" ,$data, $matches) && !empty($matches[1])) {
                if (!in_array($matches[1], ['车辆', '保险'])) {
                    $this->modelCode.=$matches[1];
                }
            }
            if (preg_match("/核[\s]*定/", $this->modelCode)) {
                if (preg_match("/险\s*?机\s*([\x7f-\xff]*[\s*\w*\.*\-*]*?[\x7f-\xff]*?)\s*厂\s*牌\s*型\s*号/", $data, $matches) && !empty($matches[1])) {
                    $this->modelCode = $matches[1];
                }
                if (preg_match("/动\s*([\x7f-\xff]*[\s*\w*\.*\-*]*[\x7f-\xff]*)\s*车\s*排\s*量/", $data, $matches) && !empty($matches[1])) {
                    $this->modelCode.=preg_replace('/\s/', '', $matches[1]);
                }
            }
        }
        //核定载客人数、核 定 载 客                5
        if (preg_match("/核[\s]*定[\s]*载[\s]*客[：]*[\s]*[：]*(\d*)[\s]*/i", $data, $matches) && !empty($matches[1])) {
            $this->vehicleSeatQuantity = $matches[1];
        }
        //核定载质量
        if (preg_match("/核定载质量[：]*[\s]*(\d*\,*\d*\.*\d*[\x7f-\xff]*)[\s\S]*?/i", $data, $matches) && !empty($matches[1])) {
            $this->vehicleDeadWeight = preg_replace('/\s+/ ', '', $matches[1]);
        }
        /**
         * 核载质量字数太长换行
         * 核定载客：3人
        克

        保险金额
         */
        if (preg_match("/核定载客[\s\S]*?人[\s]*(\S*)\s*?[保险金额]*?/i", $data, $matches) && !empty($matches[1])) {
            if (preg_match('/克|吨/', $matches[1], $matches1)) {
                $this->vehicleDeadWeight .= $matches[1];
            }
        }
        //初登日期、   初次登记日期           2013-11-26
        if (preg_match("/[初次]*登[\s]*记[\s]*日[\s]*期[：]*[\s]*(\d*[-|\/]\d*[-|\/]\d*)[\s]*/i", $data, $matches) && !empty($matches[1])) {
            $this->firstRegisterTime = $matches[1];
        }
        //起保日期、保险期间 自2021年11月14日0时0分起至2022年11月13日24时0分止 保险期间：2021 年 12 月 1 日 00:00 时起
        if (preg_match("/保险期间[\s]*[自]*[：]*[\s]*(\d+[\s]*年[\s]*\d+[\s]*月[\s]*\d+[\s]*日[\s]*\d+:\d+[\s]*时)起/i", $data, $matches) && !empty($matches[1])) {
            $startAt = str_replace(' ', '',$matches[1]);
            //            $startAt = $this->dateToTime($startAt);
            $this->startAt = $startAt;
        }
        //终保日期、保险期间 自2021年11月14日0时0分起至2022年11月13日24时0分止 至 2022 年 12 月 1 日 00:00 时止
        if (preg_match("/保险期间[\s\S]*至[\s]*(\d+[\s]*年[\s]*\d+[\s]*月[\s]*\d+[\s]*日[\s]*\d+:\d+[\s]*时)止/i", $data, $matches) && !empty($matches[1])) {
            $endAt = str_replace(' ', '',$matches[1]);
            //            $endAt = $this->dateToTime($endAt);
            $this->endAt = $endAt;
        }
        //保费金额、保险费合计（人民币大写）：                        壹仟叁佰肆拾贰元伍角叁分                                              （¥： 1,342.53                元）
        //保险费合计（人民币大写）：陆佰陆拾伍元整                                                 （¥：665.00元）

        if (preg_match("/保险费合计[\s\S]*?(\d*\,?\d+\.\d{1,2})\s*元\）/i", $data, $matches) && !empty($matches[1])) {
            $this->premium = str_replace(',', '', $matches[1]);
        }
        //险种信息
        $coveragesArr = [];
        if ($type == 'VCI') {
            //被保险人身份证号码（统一社会信用代码） 510112196902221530
            if (preg_match("/被保险人[\s\S]*?证件号[：]*(\w*)[\s]*/i", $data, $matches) && !empty($matches[1])) {
                $this->recognizeeIdCard = $matches[1];
            }
            //联系电话
            if (preg_match("/手机号：[\s]*(\S*)[\s]*?/i", $data, $matches) && !empty($matches[1])) {
                $this->recognizeeMobile = $matches[1];
            }
            //险种内容（险种名称、保额、绝对免赔、保费）
            if (preg_match("/免赔率[\s]*（元）[\s]*([\s\S]*?)[\s]*费率浮动/i", $data, $matches) && !empty($matches[1])) {
                $coverages = $matches[1];
                //                $coverages = str_replace('/', '', $coverages);
                $coverages = explode(PHP_EOL, $coverages);
                foreach ($coverages as $k => $v) {
                    if (empty($v) || $k == 0) {
                        continue;
                    }
                    $vArr = explode(' ', $v);
                    $vArr = array_filter($vArr);
                    $vArr = array_values($vArr);
                    switch (sizeof($vArr)) {
                        case '3':
                            $coveragesArr[] = [
                                'name' => $vArr[0],
                                'quotaAmount' => $vArr[1],
                                'excess' =>  '',
                                'premium' => $vArr[2],
                            ];
                            break;
                        case '4':
                            $coveragesArr[] = [
                                'name' => $vArr[0],
                                'quotaAmount' => $vArr[1],
                                'excess' => $vArr[2],
                                'premium' => $vArr[3],
                            ];
                            break;
                        case '6':
                            $coveragesArr[] = [
                                'name' => $vArr[0],
                                'quotaAmount' => $vArr[1],
                                'excess' => '',
                                'premium' => $vArr[2],
                            ];
                            $coveragesArr[] = [
                                'name' => $vArr[3],
                                'quotaAmount' => $vArr[4],
                                'excess' => '',
                                'premium' => $vArr[5],
                            ];
                            break;
                        case '7':
                            $coveragesArr[] = [
                                'name' => $vArr[0],
                                'quotaAmount' => $vArr[1],
                                'excess' => preg_match('/险/', $vArr[3]) ? '' : $vArr[2],
                                'premium' => preg_match('/险/', $vArr[3]) ? $vArr[2] : $vArr[3],
                            ];
                            $coveragesArr[] = [
                                'name' => preg_match('/险/', $vArr[3]) ? $vArr[3] : $vArr[4],
                                'quotaAmount' => preg_match('/险/', $vArr[3]) ? $vArr[4] : $vArr[5],
                                'excess' => preg_match('/险/', $vArr[3]) ? $vArr[5] : '',
                                'premium' => $vArr[6],
                            ];
                            break;
                        case '8':
                            $coveragesArr[] = [
                                'name' => $vArr[0],
                                'quotaAmount' => $vArr[1],
                                'excess' => $vArr[2],
                                'premium' => $vArr[3],
                            ];
                            $coveragesArr[] = [
                                'name' => $vArr[3],
                                'quotaAmount' => $vArr[4],
                                'excess' => $vArr[5],
                                'premium' => $vArr[6],
                            ];
                            break;
                    }
                }
            }
            $this->coverages = $coveragesArr;
            //车主姓名、VCI 车主     钟世奎
            if (preg_match("/车主[：]*[\s]*([\x7f-\xff]*\.*[\x7f-\xff]*)[\s]*/i", $data, $matches) && !empty($matches[1])) {
                $this->ownerRealname = $matches[1];
            }
        } else {
            //被保险人身份证号码（统一社会信用代码） 510112196902221530
            if (preg_match("/被保险人身份证号码[（|\(]统一社会信用代码[）|\)][\s]*(\w*)[\s]*/i", $data, $matches) && !empty($matches[1])) {
                $this->recognizeeIdCard = $matches[1];
            }
            //联系电话
            if (preg_match("/联[\s]*系[\s]*电[\s]*话[\s]*(\S*)[\s]*?/i", $data, $matches) && !empty($matches[1])) {
                $this->recognizeeMobile = $matches[1];
            }
            //整备质量  整备质量      1,625.00
            if (preg_match("/整备质量[\s\S]*?(\d*\,*\d*\.\d*)[\s\S]*?/i", $data, $matches) && !empty($matches[1])) {
                $this->vehicleWeight = str_replace(',', '', $matches[1]);
            }
            //纳税人识别号 纳税人识别号          510112196902221530
            if (preg_match("/纳税人识别号[\s]*(\w*\d*)[\s]*/i", $data, $matches) && !empty($matches[1])) {
                $this->taxNumber = $matches[1];
            }
            //车船税 当年应缴     ¥：720.00元
            if (preg_match("/当年应缴[\s\S]*?(\d*\,?\d+\.\d{1,2})\s*元?/i", $data, $matches) && !empty($matches[1])) {
                $this->taxAmount = str_replace(',', '', $matches[1]);
            }
            //往年补交
            if (preg_match("/往年补缴[\s\S]*?(\d*\,?\d+\.\d{1,2})\s*元?/i", $data, $matches) && !empty($matches[1])) {
                $this->compensateTaxAmount = str_replace(',', '', $matches[1]);
            }
            //滞纳金
            if (preg_match("/滞\s*纳\s*金[\s\S]*?(\d*\,?\d+\.\d{1,2})\s*元?/i", $data, $matches) && !empty($matches[1])) {
                $this->taxLateFee = str_replace(',', '', $matches[1]);
            }
            //完税凭证号
            if (preg_match("/完税凭证号（减免税证明号）[\s]*(\w*)[\s]*?/i", $data, $matches) && !empty($matches[1])) {
                $this->taxVoucherNo = $matches[1];
            }
            //排量 排 量 2.3540L
            if (preg_match("/排[\s]*量[\s]*(\d*\.\d*\w*)?/i", $data, $matches) && !empty($matches[1])) {
                $this->vehicleExhaustMeasure = $matches[1];
            }
            //功率 功 率 140.0000KW
            if (preg_match("/功[\s]*率[\s]*(\d*\.*\d*\w*)?/i", $data, $matches) && !empty($matches[1])) {
                $this->vehiclePower = $matches[1];
            }
            //开具税务机关
            if (preg_match("/开具税务机关\s{1,9}(\S*)1./i", $data, $matches) && !empty($matches[1])) {
                $this->taxOffice = $matches[1];
            }
        }
        return true;
    }
}