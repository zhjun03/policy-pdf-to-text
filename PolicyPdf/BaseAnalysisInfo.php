<?php

namespace PolicyPdf;

/**
 * Author:Robert
 *
 */
class BaseAnalysisInfo
{
    //保单号、
    public $policyNo;
    //支付日期、
    public $paidAt;
    //被保人、
    public $recognizee;
    //被保人证件号、
    public $recognizeeIdCard;
    //地址、
    public $recognizeeAddress;
    //联系电话、
    public $recognizeeMobile;
    //车牌号、
    public $licensePlateNo;
    //机动车种类、
    public $vehicleOwnerType;
    //使用性质、
    public $vehicleUsedNature;
    //发动机号码、
    public $engineNo;
    //车架号、
    public $vin;
    //厂牌型号、
    public $modelCode;
    //座位数、
    public $vehicleSeatQuantity;
    //核定载质量、
    public $vehicleDeadWeight;
    //初登日期、
    public $firstRegisterTime;
    //起保日期、
    public $startAt;
    //终保日期、
    public $endAt;
    //保费、
    public $premium;
    //险种内容（险种名称、保额、绝对免赔、保费）、
    public $coverages;
    //车主、
    public $ownerRealname;
    //增值服务内容（名称、次数）
    public $valueAddedServices;
    //整备质量、
    public $vehicleWeight;
    //纳税人识别号、
    public $taxNumber;
    //当年应缴(车船税)、
    public $taxAmount;
    //往年补交、
    public $compensateTaxAmount;
    //滞纳金、
    public $taxLateFee;
    //完税凭证号、
    public $taxVoucherNo;
    //排量、
    public $vehicleExhaustMeasure;
    //功率、
    public $vehiclePower;
    //开具税务机关
    public $taxOffice;
    public $type;
    public $error;

    public function dateToTime($date)
    {
        //将日期时间按照格式转换为时间数组
        $timeArr = date_parse_from_format('Y年m月d日 h:i', $date);
        $time = $timeArr['year'].'-'.$timeArr['month'].'-'.$timeArr['day'].' '.$timeArr['hour'].':'.$timeArr['minute'].':'.$timeArr['second'];
        $time = date('Y-m-d H:i:s', strtotime($time));
        return $time;
    }

    public function getInfoArr()
    {

        $data = [
            'policyNo' => $this->policyNo ?? '',
            'paidAt' => $this->paidAt ?? '',
            'recognizee' => $this->recognizee ?? '',
            'recognizeeIdCard' => $this->recognizeeIdCard ?? '',
            'recognizeeAddress' => $this->recognizeeAddress ?? '',
            'recognizeeMobile' => $this->recognizeeMobile ?? '',
            'licensePlateNo' => $this->licensePlateNo ?? '',
            'vehicleOwnerType' => $this->vehicleOwnerType ?? '',
            'vehicleUsedNature' => $this->vehicleUsedNature ?? '',
            'engineNo' => $this->engineNo ?? '',
            'modelCode' => $this->modelCode ?? '',
            'vin' => $this->vin ?? '',
            'vehicleSeatQuantity' => $this->vehicleSeatQuantity ?? '',
            'vehicleDeadWeight' => $this->vehicleDeadWeight ?? '',
            'firstRegisterTime' => $this->firstRegisterTime ?? '',
            'startAt' => $this->startAt ?? '',
            'endAt' => $this->endAt ?? '',
            'premium' => $this->premium ?? '',
            'coverages' => $this->coverages ?? '',
            'ownerRealname' => $this->ownerRealname ?? '',
            'valueAddedServices' => $this->valueAddedServices ?? '',
            'vehicleWeight' => $this->vehicleWeight ?? '',
            'taxNumber' => $this->taxNumber ?? '',
            'taxAmount' => $this->taxAmount ?? '',
            'compensateTaxAmount' => $this->compensateTaxAmount ?? '',
            'taxLateFee' => $this->taxLateFee ?? '',
            'taxVoucherNo' => $this->taxVoucherNo ?? '',
            'vehicleExhaustMeasure' => $this->vehicleExhaustMeasure ?? '',
            'vehiclePower' => $this->vehiclePower ?? '',
            'taxOffice' => $this->taxOffice ?? '',
            'type' => $this->type ?? '',
        ];
        return $data;
    }
}